#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "smg.h"
#include "key.h"
#include "led.h"
#include "oled.h"

u8   Rec;
long Rec_data[455];
long Weight_data1[150]={0};
long Weight_data2[150]={0};
long Weight_data3[150]={0};
float Avefrequence=0;

void Timer_My_Init(u16 arr,u16 psc);

void Display_Frequence(int count)	 //显示频率
{
	OLED_P8x16Str(0,4,"press");
	OLED_P8x16Ch(40,4,43);
	OLED_P8x16Str(48,4,"frequence");
	OLED_P8x16Ch(0,6,count/1000);
	OLED_P8x16Ch(8,6,count%1000/100);
	OLED_P8x16Ch(16,6,38);
	OLED_P8x16Ch(24,6,count%100/10);
	OLED_P8x16Ch(32,6,count%10);
}
void musicPlay(int i )//调用语音函数，参数为音频文件名
{    
	int n;
  char  mp3[13][8]={
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x01,0xEF},		//欢迎使用自动体外除颤器模拟教学仪，请按要求连接假人
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x02,0xEF},		//网络已连接
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x03,0xEF},		//假人已连接
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x04,0xEF},		//5秒后开始模拟
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x05,0xEF},		//正在调试，请稍后
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x06,0xEF},		//调试结束，请为假人进行心肺复苏
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x07,0xEF},		//假人心肺复苏结束，请参考图片说明，将电击片贴于假人胸部适当的位置
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x08,0xEF},		//请按下分析心率按键，分析心率
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x09,0xEF},		//请不要接触病人，正在分析心率
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x0A,0xEF},		//建议除颤
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x0B,0xEF},		//充电完毕，按下电击键除颤
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x0C,0xEF},		//放电完毕，CPR
  {0x7E,0xFF,0x06,0x03,0x00,0x00,0x0D,0xEF},		//模拟学习结束，请在手机客户端查看学习情况，模拟学习结束
  };
  for(n=0;n<8;n++)
  {
		printf("%c",mp3[i][n]);
  }
}


int main(void)
{  
	u8 key;
	delay_init(168);		//延时初始化 
	uart3_init(9600);
	uart_init(9600);	//串口初始化波特率为115200
	LED_Init();
	SMG_Init();
  KEY_Init();	        //IO初始化	
	I2C_OLED_Init();     //初始化OLED
	OLED_CLC();          //OLED屏清零
	Timer_My_Init(499,8399);     //50ms
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
	
	
	LED_Display(0x01,sng_num[0]);
	musicPlay(0);			//欢迎使用自动体外除颤器模拟教学仪，请按要求连接蓝牙，假人
	OLED_P8x16Str(0,0,"Welcome to use");
	OLED_P8x16Str(0,2,"AED CPR");
	delay_ms(5000);
	OLED_CLC(); 
	OLED_P8x16Str(0,0,"Please connect");
	OLED_P8x16Str(0,2,"dummy");
	while(1)
	{
		if(Res3==0x0E)
		{
			Send_data(USART3,0x0F);
			LED1=0;			//假人连接指示灯亮， 
			OLED_CLC(); 
			OLED_P8x16Str(0,0,"successfully");
			OLED_P8x16Str(0,2,"connected");
			musicPlay(2);			//假人已连接
			delay_ms(2000);
			break;
		}
	}
	OLED_CLC(); 
	OLED_P8x16Str(0,0," Network");
	OLED_P8x16Str(0,2,"conecting");
	while(1)
	{
		if(Res3==0x66)
		{
		//	Send_data(USART3,0x88);
			LED0=0;
			OLED_CLC(); 
			OLED_P8x16Str(0,0,"successfully");
			OLED_P8x16Str(0,2,"connected");
			musicPlay(1);			//网络已连接
			delay_ms(2000);
			break;
		}
	}
	OLED_P8x16Str(0,0,"AED Start");
	musicPlay(3);				//5秒后开始模拟
	fiveSeconds();			//数码管5秒倒计时
	musicPlay(4);       //正在调试
	smg_percentage();   //数码管显示调试进度
	musicPlay(5);       //调试结束，请为假人进行心肺复苏
	delay_ms(5000);
	OLED_CLC(); 
	OLED_P8x16Str(0,0,"CPR");
	OLED_P8x16Str(0,2,"please wait");
	while(1)
	{
		Send_data(USART3,0x10);
		if(Res3==0x11)
		{
			break;
		}
	}
	
	//第一次除颤
	musicPlay(6);		//假人心肺复苏结束，请参考图片说明，将电击片贴于患者胸部适当的位置
	OLED_CLC(); 
	OLED_P8x16Str(0,0,"CPR over");
	delay_ms(12000);
	musicPlay(7);   //请按下分析心率按键，分析心率
	OLED_CLC(); 
	OLED_P8x16Str(0,0,"Please analyse");
	OLED_P8x16Str(0,2,"the heart rate");
	delay_ms(2000);
	while(1)
	{
		key=KEY_Scan(0);
		if(key==1)
			break;
	}
	musicPlay(8);			//请不要接触病人，正在分析心率
	delay_ms(8000);
	musicPlay(9);			//建议除颤
	OLED_CLC();
	OLED_P8x16Str(0,0,"defibrillation");
	OLED_P8x16Str(0,2,"please");
	delay_ms(2000);
	smg_percentage();   //数码管显示充电进度
	musicPlay(10);			//充电完毕，按下电击键除颤
	OLED_CLC();
	OLED_P8x16Str(0,0,"discharge");
	OLED_P8x16Str(0,2,"please");
	delay_ms(2000);
	while(1)
	{
		key=KEY_Scan(0);
		if(key==2)
			break;
	}
	musicPlay(11);			//放电完毕，CPR
	OLED_CLC();
	OLED_P8x16Str(0,0,"discharge over");
	OLED_P8x16Str(0,2,"please CPR");
	delay_ms(6000);
	
	//第二次除颤
	while(1)
	{
		Send_data(USART3,0x12);
		if(Res3==0x13)
		{
			break;
		}
	}
	musicPlay(6);		//假人心肺复苏结束，请参考图片说明，将电击片贴于患者胸部适当的位置
	OLED_CLC(); 
	OLED_P8x16Str(0,0,"CPR over");
	delay_ms(10000);
	musicPlay(7);   //请按下分析心率按键，分析心率
	OLED_CLC(); 
	OLED_P8x16Str(0,0,"Please analyse");
	OLED_P8x16Str(0,2,"the heart rate");
	delay_ms(2000);
	while(1)
	{
		key=KEY_Scan(0);
		if(key==1)
			break;
	}
	musicPlay(8);			//请不要接触病人，正在分析心率
	delay_ms(8000);
	musicPlay(9);			//建议除颤
	OLED_CLC();
	OLED_P8x16Str(0,0,"defibrillation");
	OLED_P8x16Str(0,2,"please");
	delay_ms(2000);
	smg_percentage();   //数码管显示充电进度
	musicPlay(10);			//充电完毕，按下电击键除颤
	OLED_CLC();
	OLED_P8x16Str(0,0,"discharge");
	OLED_P8x16Str(0,2,"please");
	delay_ms(2000);
	while(1)
	{
		key=KEY_Scan(0);
		if(key==2)
			break;
	}
	musicPlay(11);			//放电完毕，CPR
	OLED_CLC();
	OLED_P8x16Str(0,0,"discharge over");
	OLED_P8x16Str(0,2,"please CPR");
	delay_ms(6000);
	
	while(1)
	{
		Send_data(USART3,0x14);
		if(Res3==0x15)
		{
			break;
		}
	}
	OLED_CLC();
	OLED_P8x16Str(0,0,"End of Use AED");
	musicPlay(12);			//模拟学习结束，请在手机客户端查看学习情况，模拟学习结束
	while(1);
}


