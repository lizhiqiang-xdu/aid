/*
 * I2C_OLED.h
 *
 *  Created on: 2016-5-14
 *     version: 1.1		
 *      Author: Conchsea
 * Last update: 2017-3-7
 */
#ifndef _OLED_H_
#define _OLED_H_
#include "stm32f4xx_gpio.h"
#include "delay.h"
/*************************I2C_OLED指令集***********************************
 **********************slave address 0X78*********************************
 *写数据指令0x40 *****顺序***     0x78, 0x40,	data
 *写命令指令0x00 *****顺序***     0x78, 0x00,	com
 *命令：设置对比度  0x81，com****顺序****0x78,0x00,0x81	0x78,0x00,com		com越大越亮
 *命令：关/开显示 0XAE/0XAF ****顺序***0X78,0X00,0XAE/0XAF	        AE 关  AF 开
 *命令：电荷泵开启 	8D *********顺序***0x78,0x00,0x8D	0x78,0x00,0X14		    显示时必须开启
 *命令：设置页地址 0XB0+y********顺序*****0x78,0x00,0xb0+y			y对应0到7页（第0到7行）
 *命令：设置列地址高4位((x&0xf0)>>4)|0x10)****顺序*****0X78,0X00,((x&0xf0)>>4)|0x10)	X对应列地址
 *命令：设置列地址低4位((x&0x0f)|0x01)	 ****顺序*****0X78,0X00,((x&0x0f)|0x01)		X对应列地址							X对应列地址
 *命令：设置列地址高4位0x10+x1		设置列地址低4位0x00+x0				   X1、x2分别为X的高4位及低4位
 */
#define	SlaveAddress   0x78	  //oled ADDRESS


#define SDA_Value_OLED GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_1)    //读取SDA引脚的电平值，读之前端口方向设为输入!!!!!!


extern unsigned char OLED_GRAM[128][8];	 //显存
extern void I2C_OLED_Init(void);				 //OLED初始化

void Single_Write_OLED(unsigned char REG_Address,unsigned char REG_data);	//ADDRESS WRITE DATA
void OLED_WRITE_COM(unsigned char com);				 //写命令
void OLED_WRITE_dat(unsigned char data);				 //写数据
void OLED_Set_Pos(unsigned char x,unsigned char y);	 			 //设置OLED坐标
void OLED_All(unsigned char bmp_dat);				 //OLED全屏写同一数据
void OLED_CLC(void);   				         //清屏
void OLED_P8x16Str(unsigned char x,unsigned char y, unsigned char ch[]);			 //显示8*16一组标准ASCII字符串
void OLED_P6x8Str(unsigned char x,unsigned char y, unsigned char ch[]);			 //显示6*8一组标准ASCII字符串
void OLED_P8x16Ch(unsigned char x,unsigned char y,unsigned char num);                     //在任意位置显示8*16标准ASCII字符  num为codetab.h文件中对应的字符
/*****************功能描述：显示16*16点阵  显示的坐标（x,y），y为页范围0～7****************************/
/*****************细节描述:只显示codetab.h文件中F16x16[]数组内点阵，num为显示字模在数组中的位置**********/
void OLED_P16x16Ch(unsigned char x,unsigned char y,unsigned char num);

/******功能描述：显示显示BMP图片128×64   全屏显示*********************/
void Draw_BMP(const unsigned char *BMP);

/*************注意！以下函数不可用！G2533 RAM太小********************/
void OLED_Fill(unsigned char x1,unsigned char y1,unsigned char x2,unsigned char y2,unsigned char dot);			    //画对角线
void OLED_DrawPoint(unsigned char x,unsigned char y,unsigned char t);					    //画点
void OLED_Refresh_Gram(void);							    //更新显存到LCD

#endif /* I2C_OLED_H_ */
