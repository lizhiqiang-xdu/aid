#ifndef __SMG_H
#define __SMG_H
#include "sys.h"	
#include "delay.h"


//SMG端口定义
#define DIO PAout(4)	// 串行数据输入
#define RCLK PAout(5)	// 时钟脉冲信号，上升沿有效	 
#define SCLK PAout(6)	// 打入信号，上升沿有效    

extern u8 sng_num[];

void SMG_Init(void);//初始化	
void LED_Display(u8 position,u8 number);			// 数码管显示
void LED_OUT(u8 X);					// LED单字节串行移位函数
void fiveSeconds(void);			//数码管5秒倒计时
void smg_percentage(void);  //数码管百分比进度

#endif

