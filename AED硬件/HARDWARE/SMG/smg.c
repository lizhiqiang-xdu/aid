#include "smg.h" 
#include "timer.h"
#include "delay.h"

u8 sng_num[] = {0xC0,0xF9,0xA4,0xB0,0x99,0x92,0x82,
0xF8,0x80,0x90,0x8C,0xBF,0xC6,0xA1,0x86,0xFF,0xbf};  // 0	1	2	3 4	5	6 7	8	9	A	b	C d	E F -


void SMG_Init(void)
{    	 
  GPIO_InitTypeDef  GPIO_InitStructure;

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);//使能GPIOA时钟

  //GPIOA5,A6,A7初始化设置
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_4;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;//普通输出模式
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;//推挽输出
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;//100MHz
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;//上拉
  GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化
	
	GPIO_SetBits(GPIOA,GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_4);//GPIOA9,A10设置高，灯灭

}

void LED_OUT(u8 X)
{
	u8 i;
	for(i=8;i>=1;i--)
	{
		if (X&0x80) 
			DIO=1; 
		else
			DIO=0;
		X<<=1;
		SCLK = 0;
		SCLK = 1;
	}
}

//void LED4_Display (void)  
//{
//	u8 *led_table;          // 查表指针
//	u8 i;
//	//显示第一位
//	led_table = LED_0F + LED[0];
//	i = *led_table;

//	LED_OUT(i);			
//	LED_OUT(0x01);		

//	RCLK = 0;
//	RCLK = 1;
//	//显示第二位
//	led_table = LED_0F + LED[1];
//	i = *led_table;

//	LED_OUT(i);		
//	LED_OUT(0x02);		

//	RCLK = 0;
//	RCLK = 1;
//	//显示第三位
//	led_table = LED_0F + LED[2];
//	i = *led_table;

//	LED_OUT(i);			
//	LED_OUT(0x04);	

//	RCLK = 0;
//	RCLK = 1;
//	//显示第四位
//	led_table = LED_0F + LED[3];
//	i = *led_table;

//	LED_OUT(i);			
//	LED_OUT(0x08);		

//	RCLK = 0;
//	RCLK = 1;
//}

void LED_Display(u8 position,u8 number)      //position:08,04,02,01
{
	LED_OUT(number);			
	LED_OUT(position);		
	RCLK = 0;
	RCLK = 1;
}

void fiveSeconds()			//5秒倒计时
{
     int i;
     for(i=5;i>0;i--) 
     {
      LED_Display(0x01,sng_num[i]);  
      delay_ms(1000);
     }   
}

void smg_percentage()			//数码管百分比进度
{
  int bw,sw,gw;			// 百位,十位，个位
	num=0;
	while(1)
	{
		if(num==0)
			TIM_Cmd(TIM3,ENABLE);       //使能定时器3
		if(num==101)
		{
			TIM_Cmd(TIM3,DISABLE);       //使能定时器3
			break;
		}
		bw=num/100;
		sw=num%100/10;
		gw=num%10;
		LED_Display(0x04,sng_num[bw]);
		LED_Display(0x02,sng_num[sw]); 
		LED_Display(0x01,sng_num[gw]);  
	}
}
