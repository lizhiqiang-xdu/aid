#include "timer.h"


int num;

void Timer_My_Init(u16 arr,u16 psc)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef NVIC_InitStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);    //使能定时器3时钟
	
	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;    
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;    //向上计数模式
	TIM_TimeBaseInitStructure.TIM_Period=arr;      //自动重装载值
	TIM_TimeBaseInitStructure.TIM_Prescaler=psc;    //预分频系数
	
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);   //初始化Tim3
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);    //允许定时器3更新中断
	
	//TIM_Cmd(TIM3,ENABLE);       //使能定时器3
	
	NVIC_InitStruct.NVIC_IRQChannel=TIM3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;   //抢占优先级1
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=3;          //子优先级3
	
	NVIC_Init(&NVIC_InitStruct);
}

//定时器3中断服务函数
void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==!RESET)  //溢出中断 （RESET=0）
	{
			num++;
	}
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);  //清除中断标志位
}




