#include "hx711.h"
#include "delay.h"
#include "oled.h"
#include "timer.h"

unsigned long Weight_Maopi = 0;
unsigned long Weight_Maopi_0 = 0;
long Weight_Shiwu = 0;
long myWeight_Shiwu = 0;

void Init_Hx711()
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	GPIO_InitTypeDef  GPIO_InitStructure1;

  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

  //SCK
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  //DOUT
  GPIO_InitStructure1.GPIO_Pin = GPIO_Pin_5 ;
  GPIO_InitStructure1.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure1.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure1.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(GPIOA, &GPIO_InitStructure1);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

}

unsigned long HX711_Read(void)        //增益128
{
    unsigned long count; 
    unsigned char i; 
		int ii=0;
	 // GPIO_SetBits(GPIOA, GPIO_Pin_5);
		GPIO_ResetBits(GPIOA, GPIO_Pin_4); //模块没准备好时，CLK线上输出低电平
    count=0;
    while(1)
        {
            if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_5)==0)
                {
                        delay_us(1);
                        if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_5)==0)
                        {
                                break;
                        }                        
                }                       
                ii++;
                if(ii>=1000000)
                {
                        break;
                }
        }
    delay_us(1);
    for(i=0;i<24;i++)
    {                         
				GPIO_SetBits(GPIOA, GPIO_Pin_4); //时钟高电平
        delay_us(1);
        count=count<<1; 
				delay_us(1);
				GPIO_ResetBits(GPIOA, GPIO_Pin_4); //时钟低电平
        if(GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_5)==1)//读入数据
          {
             count++; 
          }                                       
    } 
    GPIO_SetBits(GPIOA, GPIO_Pin_4); 
    delay_us(2);
	  count=count^0x800000;//第25个脉冲下降沿来时，转换数据
    GPIO_ResetBits(GPIOA, GPIO_Pin_4);	
    return(count);
}

void Get_Maopi()  //获取毛皮重量
{
	do
	{
		Weight_Maopi_0 = HX711_Read();
		delay_ms(100);
		Weight_Maopi = HX711_Read();
		OLED_P8x16Str(0,0,"please wait");
//		OLED_P8x16Ch(48,0,43);
//		OLED_P8x16Str(56,0,"");
		OLED_P8x16Str(0,2,"self checking");
//	  OLED_P8x16Ch(32,2,43);
//	  OLED_P8x16Str(40,2,"");
		//
	}
  while(Weight_Maopi/GapValue!=Weight_Maopi_0/GapValue);
} 

void Display_Weight()	 //显示重量，单位Kg
{
	OLED_P8x16Str(0,0,"the weigh");
//	OLED_P8x16Ch(24,0,43);
//	OLED_P8x16Str(32,0,"");
	OLED_P8x16Ch(0,2,Weight_Shiwu/1000);
	OLED_P8x16Ch(8,2,Weight_Shiwu%1000/100);
	OLED_P8x16Ch(16,2,-2);
	OLED_P8x16Ch(24,2,Weight_Shiwu%100/10);
	OLED_P8x16Ch(32,2,Weight_Shiwu%10);
	OLED_P8x16Ch(40,4,press_count);
}

void Get_Weight()
{
	long Weight_Shiwu0=0;
	Weight_Shiwu0 = HX711_Read();
	Weight_Shiwu0 = Weight_Shiwu0- Weight_Maopi;		//获取净重	
	Weight_Shiwu0 = (unsigned int)((float)Weight_Shiwu0/GapValue); 	//计算实物的实际重量
	Weight_Shiwu=Weight_Shiwu0/4.38;       //  *10*0.5/1.75/1.1;
	Display_Weight();
	delay_ms(100);
}




