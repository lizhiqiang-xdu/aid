#ifndef __HX711_H
#define __HX711_H
#include "sys.h"

#define GapValue 210.6f

unsigned long HX711_Read(void);
void Get_Maopi(void);
void Display_Weight(void);
void Get_Weight(void);
void Init_Hx711(void);

extern unsigned long Weight_Maopi_0;
extern unsigned long Weight_Maopi;
extern long Weight_Shiwu;
extern long myWeight_Shiwu;

#endif


