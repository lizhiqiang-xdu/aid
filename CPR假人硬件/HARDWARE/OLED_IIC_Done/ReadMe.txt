Creat on:2017-3-7
Author: conchsea
Explanation： 本代码为OLED控制程序，使用软件I2C协议进行数据传输。适用于stm32F407
Note ：本代码依赖于myiic.c文件
Last update:2017-3-7


调用顺序
   delay_init(168); //初始化系统延时函数
   I2C_OLED_Init();  
   //显示程序


其他说明：
*******程序可以调用的显示函数***********
void OLED_P8x16Str(u8 x,u8 y, u8 ch[]);			 //在任意位置显示8*16一组标准ASCII字符串
void OLED_P6x8Str(u8 x,u8 y, u8 ch[]);			 //在任意位置显示6*8一组标准ASCII字符串
void OLED_P8x16Ch(u8 x,u8 y,u8 num);                     //在任意位置显示8*16标准ASCII字符  num为codetab.h文件中对应的字符
void OLED_P16x16Ch(u8 x,u8 y,u8 num);                    //在任意位置显示16*16文字字符，需要显示其他字符需要自己提取
还可显示一些简单图片点阵，但需要大容量存储

***********其他注意事项***************
1.  u8表示unsigned char
2.  主程序中不用包含codetab.h  否则会报错。
3.  在调用显示函数时，注意 X对应 列显示  Y对应 行显示
   Y的选择范围为0-7，对于8*什么字符可以显示8行  16*的字符可以显示4行
   X的选择范围是0-128，根据显示字符的不同，需要自己调节显示的位置。
   例如显示8*16的字符：
	OLED_P8x16Ch(0,0,6);
	OLED_P8x16Ch(8,0,6);
	OLED_P8x16Ch(16,0,6);
	OLED_P8x16Ch(24,0,6);  （既X Y 的行列显示略有不同，需要注意。）
4.显示位置可以直接被下次显示数据所覆盖，不需要清除
