#include "timer.h"
#include "hx711.h"
#include "led.h"


float Press_time=0.0;
int press_count=0; //记录按压次数
int num;
long Weight_data1[120]={0};
long Weight_data2[120]={0};
long Weight_data3[120]={0};
int voice_num=0,i=0;

//定时器3初始化
void Timer_My_Init(u16 arr,u16 psc)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	NVIC_InitTypeDef NVIC_InitStruct;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE);    //使能定时器3时钟
	
	TIM_TimeBaseInitStructure.TIM_ClockDivision=TIM_CKD_DIV1;    
	TIM_TimeBaseInitStructure.TIM_CounterMode=TIM_CounterMode_Up;    //向上计数模式
	TIM_TimeBaseInitStructure.TIM_Period=arr;      //自动重装载值
	TIM_TimeBaseInitStructure.TIM_Prescaler=psc;    //预分频系数
	
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);   //初始化Tim3
	
	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);    //允许定时器3更新中断
	
//	TIM_Cmd(TIM3,ENABLE);       //使能定时器3
	
	NVIC_InitStruct.NVIC_IRQChannel=TIM3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;   //抢占优先级1
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=3;          //子优先级3
	
	NVIC_Init(&NVIC_InitStruct);
}

//定时器3中断服务函数
void TIM3_IRQHandler(void)
{
	if(TIM_GetITStatus(TIM3,TIM_IT_Update)==!RESET)  //溢出中断 （RESET=0）
	{
		Press_time=Press_time+1.0f;
		if(voice_num==1)
		{
			Weight_data1[i++]=Weight_Shiwu;
			if(i==120)i=0;
		}
		if(voice_num==2)
		{
			Weight_data2[i++]=Weight_Shiwu;
			if(i==120)i=0;
		}
		if(voice_num==3)
		{
			Weight_data3[i++]=Weight_Shiwu;
			if(i==120)i=0;
		}
	}
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);  //清除中断标志位
}

