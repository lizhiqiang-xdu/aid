#ifndef __LED_H
#define __LED_H
#include "sys.h"

//////////////////////////////////////////////////////////////////////////////////	 
//STM32F4工程模板-库函数版本
//淘宝店铺：http://mcudev.taobao.com									  
////////////////////////////////////////////////////////////////////////////////// 	

//N21自启动端口
#define start_N21 PEout(11)

//LED端口定义
#define LED0 PAout(6)	//黄灯，指示AED
#define LED1 PAout(7)	// 红灯 
#define LED2 PAout(2) // 绿灯

//语音端口定义
#define voice1 PAout(12)
#define voice2 PAout(11)
#define voice3 PAout(8)
#define voice4 PCout(9)
#define voice5 PAout(15)

//呼吸端口定义
#define breath PCout(0)

void LED_Init(void);//初始化		 				    
#endif


