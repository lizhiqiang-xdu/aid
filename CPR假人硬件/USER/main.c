#include "stm32f4xx.h"
#include "hx711.h"
#include "OLED.h"
#include "myiic.h"
#include "adc.h"
#include "usart.h"
#include "timer.h"
#include "led.h"
#include "key.h"

int signal=0,Net_flag=1;

void Display_Frequence(int count)	 //显示频率
{
	OLED_P8x16Str(0,4,"press frequence");
	OLED_P8x16Ch(0,6,count/1000);
	OLED_P8x16Ch(8,6,count%1000/100);
	OLED_P8x16Ch(16,6,-2);
	OLED_P8x16Ch(24,6,count%100/10);
	OLED_P8x16Ch(32,6,count%10);
}

int main(void)
{
	u8 zu=0,aaa=0,i;     //接收AED信号
	u8 key=0;
	float Press_frequence1=0,Press_frequence2=0,Press_frequence3=0,Avefrequence=0;//记录按压频率
	
	/*初始化*/
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
	delay_init(168);     //初始化延时函数
	I2C_OLED_Init();     //初始化OLED
	uart_init(115200);     //初始化串口
	uart3_init(9600);     //初始化串口
	OLED_CLC();          //OLED屏清零
	Timer_My_Init(999,8399);     //10ms
	LED_Init();
	KEY_Init();
	Init_Hx711();
	
	/*开始急救*/
	voice1=0;      //语音1,提示开始
	Get_Maopi();   // Get_Weight();
	OLED_CLC();          //OLED屏清零
	voice1=1;
	
//	while(1)
//	{
//		
//	}
	//检测网络是否连接
	printf("AT+CLOUDSTATE?\r\n");
	delay_ms(200);
	while(1)
	{
		if(USART_RX_BUF[12]=='1')
		{
			LED0=~LED0;
			OLED_P8x16Str(0,2,"MQTT suc");       //char authentication[50]="+CLOUDHDAUTH: OK";          //+CLOUDSUBACK: <OK>
			USART_RX_BUF[12]=0;
			Net_flag=1;
			break;   
		}
		else
		{
			Net_flag=0;
			break; 
		}
	}	
	//网络未连接时自启动
	while(Net_flag==0)
	{
		while(1)  //N10模块开机
		{
			delay_ms(3000);
			start_N21=0;
			delay_ms(2000);
			start_N21=1;
			delay_ms(25000);
			break;
		} 
		printf("ATE0\r\n");   //N10关闭回显
		while(1)
		{
			key=KEY_Scan(0);
			if(USART_RX_BUF[1]=='K' || key==1)
			{
				LED0=~LED0;
				OLED_P8x16Str(0,2,"qufan suc");      
				USART_RX_BUF[1]=0;
				break;   
			}
		//	else OLED_P8x16Str(0,2,"qufan fail");
		}	
		printf("AT+CLOUDHDAUTH=a15HesmUcmC,nwytest01,RLt4nHO5r4fmcue79tlmddervSiPCeeo\r\n");   //N10设备鉴权
		while(1)
		{
			key=KEY_Scan(0);
			if(USART_RX_BUF[1]=='K' || key==1)
			{
				OLED_P8x16Str(0,2,"jianquan ing");
				while(1)
				{
					if(((USART_RX_BUF[14]=='O')&&(USART_RX_BUF[6]=='H')) || key==1)
					{
						LED0=~LED0;
						OLED_P8x16Str(0,2,"jianquan suc");
						USART_RX_BUF[14]=0;
						USART_RX_BUF[6]=0;
						break; 
					}			
				}	
				break;				
			}
		//	else  OLED_P8x16Str(0,2,"jianquan fail");
		}	
		printf("AT+CLOUDCONN=60,0,4\r\n");    //N10建立MQTT连接
		while(1)
		{
			key=KEY_Scan(0);
			if(USART_RX_BUF[1]=='K' || key==1)
			{
				LED0=~LED0;
				OLED_P8x16Str(0,2,"lianjie suc");
				USART_RX_BUF[1]=0;
				break;   
			}
		//	else OLED_P8x16Str(0,2,"lianjie fail");
		}	
		printf("AT+CLOUDSUB=/a15HesmUcmC/nwytest01/user/update,0\r\n");   //N10订阅Topic
		while(1)
		{
			key=KEY_Scan(0);
			if(((USART_RX_BUF[15]=='O')&&(USART_RX_BUF[6]=='S')) || key==1)
			{
				LED0=~LED0;
				OLED_P8x16Str(0,2,"dingyue suc");
				USART_RX_BUF[6]=0;
				USART_RX_BUF[15]=0;
				break;   
			}
		//	else OLED_P8x16Str(0,2,"dingyue fail");
		}	
		break;
	}
		//连接AED
		OLED_CLC();
		while(1)
		{ 
			OLED_P8x16Str(0,0,"AED");
			OLED_P8x16Str(0,2,"conecting");
			Send_data(USART3,0x0E);
			if(Res3==0x0F)
			{
				LED0=0;   //亮黄灯，表示AED已连接
			}
			break;
		}
		OLED_CLC();
		while(1)    //告诉AED网络连接成功
		{
			Send_data(USART3,0x66);
//			if(Res3==0x88)
//			{
//				break;   
//			}
			if(Res3==0x10)
				break;
		}
		/*第一组*/
		OLED_CLC();
		while(1)
		{
				if(voice_num==0)
				{
					voice2=0;  //语音2，开始挤压胸部
					delay_ms(5000);
					voice2=1;
					voice_num++;
				}
				Get_Weight();  //获得按压力度
				if((Weight_Shiwu/100.f)<10)  num=6;
				if((Weight_Shiwu/100.f)>10) //判断是否有按压
				{
						if(press_count==0)       
						TIM_Cmd(TIM3,ENABLE);   //第一次按压，打开定时器，开始计时
						if(num==6)
						press_count++;
						num=0;
						LED1=0;  //检测到按压亮红灯
						if((Weight_Shiwu/100.f)>13&&(Weight_Shiwu/100.f)<16)
						{ 
							LED2=0;  //按压力度合适，亮绿灯
						}
						if(press_count==10)                      
						{		
								TIM_Cmd(TIM3,DISABLE);  //第30次按压，关闭定时器，停止计时 
								Press_time=Press_time*0.1f;   //计算按压30的时间
								Press_frequence1=10.0f/Press_time;    //计算按压30次的频率
								voice3=0;     //语音3，提示开始人工呼吸
								delay_ms(3000);
							  voice3=1;
								LED1=0;       //红灯再次亮，等待吹气
								while(1)
								{
										aaa=GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0);
										if(aaa==1) break;
								} 
								if(zu==0&&aaa==1)   //GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0)
		            {
				            LED0=1;   //吹气成功，灭灯
										zu=1;
										voice5=0;     //语音5，请使用AED
										delay_ms(5000);
										voice5=1;
								} 
						}
				}
				else  { LED1=1; LED2=1; }  //按压不在范围的时候灭红灯
			  if(press_count==10)	 {press_count=0;break;} 
		}
		while(1)
		{
			Send_data(USART3,0x11);  //给AED信号
			if(Res3==0x12)      //AED给信号则开始工作
				break;
		}	
		/*第二组*/
		while(1)
		{
				LED0=0;   //亮黄灯，表示AED已连接
				if((Weight_Shiwu/100.f)>13&&(Weight_Shiwu/100.f)<16)
				{ 
					LED2=0;  //按压力度合适，亮绿灯
				}
				if(voice_num==1)
				{
					voice4=0;  //语音4，继续挤压胸部
					delay_ms(2000);
					voice4=1;
					voice_num++;
				}
				Get_Weight();  //获得按压力度
				if((Weight_Shiwu/100.f)<10)  num=6;
				if((Weight_Shiwu/100.f)>10) //判断是否有按压
				{
						if(press_count==0)       
						TIM_Cmd(TIM3,ENABLE);   //第一次按压，打开定时器，开始计时
						if(num==6)
						press_count++;
						num=0;
						LED1=0;  //检测到按压亮红灯
						if(press_count==0)       
						TIM_Cmd(TIM3,ENABLE);   //第一次按压，打开定时器，开始计时
						if(press_count==10)                      
						{		
								TIM_Cmd(TIM3,DISABLE);  //第30次按压，关闭定时器，停止计时 
								Press_time=Press_time*0.1f;   //计算按压30的时间
								Press_frequence2=10.0f/Press_time;    //计算按压30次的频率
								voice3=0;     //语音3，提示开始人工呼吸
								delay_ms(3000);
								voice3=1;
								LED0=0;       //红灯再次亮，等待吹气
								while(1)
								{
										aaa=GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0);
										if(aaa==1) break;
								} 
								if(zu==1&&aaa==1)   //GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0)
		            {
										LED0=1;   //吹气成功，灭灯
				            voice5=0;     //语音5，请使用AED
										delay_ms(5000);
										voice5=1;
										Send_data(USART3,4);  //给AED信号
				            zu=2;
	              } 
						}
				}
				else  { LED1=1; LED2=1; }  //按压不在范围的时候灭红灯
				if(press_count==10)	 {press_count=0;break;} //
		}
		while(1)
		{
			Send_data(USART3,0x13);  //给AED信号
			if(Res3==0x14)      //AED给信号则开始工作
			break;
		}
		/*第三组*/
		while(1)
		{
				LED0=0;   //亮黄灯，表示AED已连接
				if(voice_num==2)
				{
					voice4=0;  //语音4，继续挤压胸部
					delay_ms(2000);
					voice4=1;
					voice_num++;
				}
				Get_Weight();  //获得按压力度
				if((Weight_Shiwu/100.f)<10)  num=6;
				if((Weight_Shiwu/100.f)>10) //判断是否有按压
				{
						if(press_count==0)       
						TIM_Cmd(TIM3,ENABLE);   //第一次按压，打开定时器，开始计时
						if(num==6)
						press_count++;
						num=0;
						LED1=0;  //检测到按压亮红灯
						if((Weight_Shiwu/100.f)>13&&(Weight_Shiwu/100.f)<16)
						{ 
							LED2=0;   //按压力度合适亮绿灯
						}
						if(press_count==0)       
						TIM_Cmd(TIM3,ENABLE);   //第一次按压，打开定时器，开始计时
						if(press_count==10)                      
						{		
								TIM_Cmd(TIM3,DISABLE);  //第30次按压，关闭定时器，停止计时 
								Press_time=Press_time*0.1f;   //计算按压30的时间
								Press_frequence3=10.0f/Press_time;    //计算按压30次的频率
								voice3=0;     //语音3，提示开始人工呼吸
								delay_ms(3000);
								LED0=0;       //红灯再次亮，等待吹气
								while(1)
								{
										aaa=GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0);
										if(aaa==1) break;
								} 
								if(zu==2&&aaa==1)   //GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0)
		            {
										LED0=1;   //吹气成功，灭灯
										zu=3;
										voice5=0;     //语音5，请使用AED
									  delay_ms(5000);
										Send_data(USART3,6);  //给AED信号
		            } 
						}
				}
				else  { LED1=1; LED2=1; }  //按压不在范围的时候灭红灯,按压力度不合适灭绿灯
				if(press_count==10)	 {press_count=0;break;}
		}
		if(zu==3)
		{
				Avefrequence=(Press_frequence1+Press_frequence2+Press_frequence3+0.3f)/3.0f;
				Avefrequence=(Avefrequence)*100/1;
				Display_Frequence(Avefrequence);
		}
		Avefrequence=Avefrequence/100.0f;
		printf("AT+CLOUDPUB=/a15HesmUcmC/nwytest01/user/update,0,{Avefrequence:%f}\r\n",Avefrequence);
		delay_ms(20);
		for(i=0;i<120;i++)
		{
			printf("AT+CLOUDPUB=/a15HesmUcmC/nwytest01/user/update,0,{press:%ld}\r\n",Weight_data1[i]);
			delay_ms(20);
		}
		for(i=0;i<120;i++)
		{
			printf("AT+CLOUDPUB=/a15HesmUcmC/nwytest01/user/update,0,{press:%ld}\r\n",Weight_data2[i]);
			delay_ms(20);
		}
		for(i=0;i<120;i++)
		{
			printf("AT+CLOUDPUB=/a15HesmUcmC/nwytest01/user/update,0,{press:%ld}\r\n",Weight_data3[i]);
			delay_ms(20);
		}
		while(1);
}


